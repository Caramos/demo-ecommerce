import CarruselHome from '../../support/carrusel_class.spect'

describe('revisar menu de categorias principal', () => {
    it('el menú de categorías se presenta completo, ordenado y con links asociados', () => {
        cy.visit('/index.html')
        cy.check_menu(true)
    })
})

describe('Comportamiento y contenido banners principales', () => {
    
    it('se muestra la estructura esperada en los banners principales', () => {
        const carruselObject = new  CarruselHome()
        carruselObject.verificarDataBanners()
    })
    it('se muestra solo el primer banner como visible al cargar la página',() => {
        const carruselObject = new  CarruselHome()
        carruselObject.esPrimerBannerVisibleDefault()
    })

    it('transición automatica de banners principales', () => {
        const carruselObject = new  CarruselHome()
        carruselObject.verificarTranscioAutoBanners()
    })

    it('cambiar manualmente banners hacia adelante', () => {
        const carruselObject = new  CarruselHome()
        carruselObject.secuenciaCambioBanners(0,'adelante',10)
    })

    it('cambiar manualmente banners hacia atras', () => {
        const carruselObject = new  CarruselHome()
        carruselObject.secuenciaCambioBanners(0,'atras')
    })
})