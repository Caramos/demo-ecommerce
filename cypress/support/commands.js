// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//

var fixtures_path = '';

// use is_strict to do check case sensitive 
Cypress.Commands.add('check_menu', (is_strict) => {
    const TITLE_MENU_PRINCIPAL = 'CATEGORIES'

    if(typeof is_strict === 'boolean')
        cy.get('div.list-group #cat')
        .should('be.visible')
        .should(($title) => {
            if(is_strict)
                expect($title.text()).equal(TITLE_MENU_PRINCIPAL);
            else 
                expect($title.text().toLowerCase()).equal(TITLE_MENU_PRINCIPAL.toLowerCase());
        })
    else
        throw "Categories menu: is_strict is not valid"

    cy.fixture(fixtures_path + '/categories.json')
        .then((json) => {
            (json.categorias).forEach((elemento_esperado,index) => {
                cy.get('div.list-group a#itemc.list-group-item')
                    .then(($elemento_recibido) => {
                        let elemento_recibido = $elemento_recibido[elemento_esperado.index]
                        expect(elemento_recibido.text).to.equal(elemento_esperado.name)
                        cy.get(elemento_recibido).invoke('attr','href').should('eq','#')
                        cy.get(elemento_recibido).invoke('attr','onclick').should('eq',"byCat('" + elemento_esperado.destino + "')")
                        cy.get(elemento_recibido).should('be.visible')
            })
        })
    })
    
}) 