/**
 * Maneja las pruebas para el carrusel insertado en el home del sitio
 */

import banners_esperados from '../fixtures/banners_home.json'

class CarruselHome {
    /**
     *  Identifica y recupera el bloque contenedor del carrusel
     */
    constructor () {
        cy.get('div#carouselExampleIndicators').as('contenedorBanners')
        cy.get('@contenedorBanners').find('.carousel-inner .carousel-item').as('banners')
        cy.get('@contenedorBanners').find('a.carousel-control-next span.carousel-control-next-icon').as('controlAdelante')
        cy.get('@contenedorBanners').find('a.carousel-control-prev span.carousel-control-prev-icon').as('controlAtras')
        this.tiempoTransicionBanners = 4200 // Tiempo de transacion para desplazar los banners. Transicion = 4000 ms
        this.indice_maximo = banners_esperados.banners.length;
    }

    verificarDataBanners() {
        cy.get('@banners').then(($banners) => {
            $banners.each((index) => {
                let banner_recibido = $banners[index]
                let banner_esperado = banners_esperados.banners[index]
                cy.get(banner_recibido).find('img').invoke('attr','src').should('eq',banner_esperado.image)
                cy.get(banner_recibido).find('img').invoke('attr','alt').should('eq',banner_esperado.alt)
                cy.get(banner_recibido).find('img').should('not.have.attr','href')
                /* otra forma
                .should('contain', 'Edit User') // yields <a>
                .and('have.attr', 'href') // yields string value of href
                .and('match', /users/) // yields string value of href
                .and('not.include', '#') // yields string value of href
                */
            })
        })
    }

    esPrimerBannerVisibleDefault() {
        cy.get('@banners').then(($banners) => {
            cy.get($banners[0]).find('img').should('be.visible')  
            $banners.each((index) => {
                let banner_recibido = $banners[index]
                if(index > 0)
                    cy.get(banner_recibido).find('img').should('be.not.visible')
            })
        })
    }

    verificarTranscioAutoBanners() {
        cy.visit('/index.html')
        cy.get('@banners').then(($banners) => {
            var tiempo_espera = 0
            $banners.each((index) => {
                cy.wait(tiempo_espera).then(() => {
                    let banner_recibido = $banners[index]
                    cy.get(banner_recibido).find('img').should('be.visible')     
                    cy.get(banner_recibido).should('have.class', 'carousel-item active')

                    cy.get('@contenedorBanners').find('div.carousel-item.active').its('length')
                        .should('eq',1) // Solo se espera 1 elemento con la clase activa
                })
                tiempo_espera = this.tiempoTransicionBanners // settea la transcion a partir del segundo banner
            })
        })
    }

    cambiarBanner(index_actual,movimiento) { 
        let indice_maximo = this.indice_maximo - 1; // para que regrese el indice al inicio
        let indice_minimo = 0
        let indice_siguiente = 0

        if(movimiento === 'adelante') {
            if(index_actual === indice_maximo)
                indice_siguiente = indice_minimo
            else 
                indice_siguiente = index_actual + 1
                cy.get('@controlAdelante').click()
        }
        else if(movimiento === 'atras') {
            if(index_actual === indice_minimo)
                indice_siguiente = indice_maximo
            else 
                indice_siguiente = index_actual - 1
            cy.get('@controlAtras').click()
        }
        else
            throw 'movimiento ' + movimiento + ' no esperado'

    
        cy.get('@banners').then(($banners) => {
            let banner_esperado = banners_esperados.banners[indice_siguiente]
            let banner_recibido = $banners[indice_siguiente]
            console.log("index actual " + index_actual)
            console.log("siguiente index " + indice_siguiente)
            cy.get(banner_recibido).find('img').should('be.visible')  
            cy.get(banner_recibido).find('img').invoke('attr','src').should('eq',banner_esperado.image)
            cy.get(banner_recibido).find('img').invoke('attr','alt').should('eq',banner_esperado.alt)
            cy.get(banner_recibido).should('have.class', 'carousel-item active')
            cy.get('@contenedorBanners').find('div.carousel-item.active').its('length')
                .should('eq',1) // Solo se espera 1 elemento con la clase activa
        })
        return indice_siguiente
    }

    secuenciaCambioBanners(inicio,movimiento,repeticiones) {
        /** 
            inicio = indice inicial donde se comenzara a recorrer los banners
        */
        cy.visit('/index.html')
        let limite = this.indice_maximo
        let inicia_secuencia = inicio
        repeticiones === undefined ? repeticiones = limite : repeticiones = repeticiones

        for (let index = 0; index < repeticiones; index++) {
            inicia_secuencia = this.cambiarBanner(inicia_secuencia,movimiento)
        }

    }
}

export default CarruselHome;